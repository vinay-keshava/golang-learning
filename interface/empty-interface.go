package main

import "fmt"

func main() {

	var i interface{} // empty interface
	/*

		Empty interfaces are handled by those where it handles of unknown type
		for example fmt.Println is an empty interface which handles unknown type
	*/
	i = 45
	fmt.Printf("%v %T\n", i, i)

	i = "hello buddy"
	fmt.Printf("%v %T", i, i)

}
