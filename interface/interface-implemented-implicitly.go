package main

import "fmt"

type I interface {
	interfaceFunctionSignature(x int, y float64)
}

type T struct {
	S string
}

func (t T) interfaceFunctionSignature(x int, y float64) {
	fmt.Println(t.S)
}

// Interface are implemented implicitly without using the implements keyword
func main() {

	var i I = T{"hello"}
	//creating a variable type of interface I which is related to the type struct T
	i.interfaceFunctionSignature(10, 20)
}
