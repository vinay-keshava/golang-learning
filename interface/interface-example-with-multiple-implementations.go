package main

import "fmt"

type I interface {
	interfaceFunctionSignature()
}
type F float64 //aliasing the float64 datatype to F using type keyword
type T struct {
	S string
}

//here we implement the interface twice in two different scenarios it is implicitly called based on receiver types

func (t *T) M() {
	fmt.Println(t.S)
}
func (f F) M() {
	fmt.Println(f)
}
func main() {
	i := &T{"Hello buddy"}
	fmt.Printf("%v,%T", i, i)
	fmt.Println()
	i.M()

	j := F(3.12)
	fmt.Printf("%v,%T", j, j)
	j.M()

}
