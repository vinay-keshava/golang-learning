package main

import (
	"fmt"
	"math"
)

type rect struct {
	length  float64
	breadth float64
}
type circle struct {
	radius float64
}

type geometry interface {
	area() float64
	perim() float64
}

func (r rect) area() float64 {
	return r.length * r.breadth
}
func (r rect) perim() float64 {
	return 2*r.length + 2*r.breadth
}

func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}
func (c circle) perim() float64 {
	return 2 * math.Pi * c.radius
}

func calc(geo geometry) {
	fmt.Println(geo.area())
}
func main() {
	/*
		an interface type is a named collection of method signature to implement abstraction

	*/
	c1 := circle{34.00}
	calc(c1)
}
