/*
Type assertion in interface associated with the type
checks whether the type associated with that interface is true or not

*/

package main

import "fmt"

type Student struct {
	S string
}
type I interface {
	interfaceFunctionSignature()
}

func (stud Student) interfaceFunctionSignature() {
	fmt.Println(stud.S)
}

func interfaceImpl(exampleI I) {
	exampleI.interfaceFunctionSignature()
}
func main() {
	var i I
	i = Student{"vinay"}
	interfaceImpl(i)
	// here asserting that i is a type of Student
	fmt.Println(i.(Student))

	if s, ok := i.(Student); ok {
		fmt.Println("The type is of the type struct Student Asserting that it is a type of Student", s.S)
	} else {
		fmt.Println("This type is other than Student", s.S)
	}
}
