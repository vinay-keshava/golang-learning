package main

import "fmt"

func main() {
	fmt.Println("vim-go")

	var randomInteger int = 123
	randomFloat := float64(randomInteger)

	fmt.Println(randomFloat)
}
