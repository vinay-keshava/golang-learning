package main

import "fmt"

func main() {
	var int_array [10]int
	fmt.Println("vim-go")
	fmt.Println(int_array)

	int_array[0] = 200
	int_array[3] = 400
	fmt.Println(int_array)

	string_array := []string{"Vinay", "Keshava"}
	fmt.Println(string_array)
}
