package main

import "fmt"

type Student struct {
	Name string
	Usn  string
	Addr Address
}

type Address struct {
	HouseNo  int
	Landmark string
	City     string
	Pincode  int
}

func main() {
	fmt.Println("vim-go")
	vinay := Student{Name: "Vinay", Usn: "4AL19CS041", Addr: Address{HouseNo: 8, Landmark: "Manipal Hospitals", City: "Bengaluru", Pincode: 560024}}
	fmt.Println(vinay)
}
