package main

import "fmt"

type Student struct {
	Name     string
	usn      int
	Location // here the Location struct is embedded
}
type Location struct {
	Pincode  int
	Landmark string
	DoorNum  int
}

func main() {
	fmt.Println("vim-go")
	vinay := Student{Name: "vinay", usn: 123, Location: Location{
		Pincode:  560024,
		Landmark: "Temple",
		DoorNum:  90,
	}}
	fmt.Println(vinay)
}
