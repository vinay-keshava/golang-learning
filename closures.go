package main

import "fmt"

func main() {
	fmt.Println("vim-go")
	a := 0
	sample_anonymous_function := func() int {
		a += 1
		return a
	}
	fmt.Println(sample_anonymous_function())
	fmt.Println(sample_anonymous_function())

	//closure are like anonymous function without having to define a name to it something like inline function

}
