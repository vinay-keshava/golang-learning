package main

import "fmt"

func main() {
	fmt.Println("vim-go")
	randomArray := [5]int{1, 2, 3, 4, 5}
	fmt.Println(randomArray)

	slicedVariable := randomArray[0:3]
	fmt.Println(slicedVariable)
	fmt.Println(slicedVariable)

	//slices are like references to underlying arrays changing the values of slice will also change the value of array which was referenced.

	subjects := [5]string{"Design and analysis of Algorithms", "MAD", "Big Data", "IOT", "data Science"}
	EighthSemSub := subjects[2:4]
	fmt.Println(EighthSemSub)
	EighthSemSub[1] = "Computer Organization"
	fmt.Println(subjects) // here IOT has been changed to Computer Organization which says slices are referenced by arrays changing slices will also change the array members

	q := []int{1, 54, 54, 67, 43}
	fmt.Println(q)

	// creating a slice literal with struct
	s := []struct {
		i int
		b bool
	}{
		{3, false},
		{43, true},
	}
	fmt.Println(s)

	//extending slice length
	a := []int{23, 34, 4, 56} // internally the slice creates an arrays which references so even if slice elements are removed capacity is shown by array created internally
	fmt.Println(a)
	a = a[:4] //slice length extended
	fmt.Println(len(a))
	fmt.Println(cap(a))

	a = a[:0]
	fmt.Println(len(a), cap(a))

	//nil slices
	var abc []int
	fmt.Println(abc, len(abc), cap(abc))
	if abc == nil {
		fmt.Println("Nil value of slice")
	}

	//creating a slice with builtin make function
	//syntax for make is make([]T,length,capacity)
	xyz := make([]int, 5)
	fmt.Println(xyz, len(xyz), cap(xyz))
	xyz = make([]int, 5, 20)
	fmt.Println(xyz, len(xyz), cap(xyz))

	//appending to slice
	abcd := []int{23, 4, 56, 6, 7, 7}
	fmt.Println(abcd)
	abcd = append(abcd, 45, 6, 7, 889)
	fmt.Println(abcd)

	//ranging over the slice
	for _, v := range abcd {
		fmt.Print(v)
	}
}
