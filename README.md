## Golang Learning 

## Interfaces
```
In Golang there is no need to explicity mention that it is implementing a interface like in java (implements interface_name)
go does this automatically, it automatically detects and checks whether all the requirements are satisfying for the interface and automatically implements it
```
## Methods in Golang 

```
Methods in Golang are of Two types
Value Receiver Method -> where the value receive a copy of the parameters
Pointer Receiver Method -> here updating the value will be updated at original place too similar to call by reference

```
## Arrays & Slices
Arrays in Golang
```
Collection of homogenous type of elements
[n]T is a type of array where n is the size of the array where T is the datatype
An example for var i [10]int
create an array named i with the type int

prime_number:=[10]int{12,0,2,23,45,56,64} -> this is an example of array shorthand

```
Slices in Golang
```
Slices in golang are similar to that of arrays with dynamic size compared to arrays
Slices consists of underlying array and slice is referenced by the array internally
similarly the size and capacity can be checked
```

## Structs

Structs are a collection of different types of data like a collection of variables of different datatypes

The struct members are accessed by the dot operator 
 
Struct Pointers can be used to point to the struct and also using the struct pointer is same as struct member

Struct literals to set each member of the struct 

## Pointers
```
//Same as in C with reference and dereference operator like "&" and "*"
p:=300
i:=&p //reference operator

p -> gives the address of the variable
&i -> gives the address
*i -> gives the value

```
## Defer

Defer statement is evaluated immediately but executed at the last - in the manner of Stack Last In First Out Order

```
func main(){
defer fmt.Println("World")
fmt.Println("HEllo")
}
```

## Looping Constructs

There's only one looping statement in go which is 'for' , for is go's while loop too.

-> a short statement can be written within the if statement and the scope of that local variable is within the if and else block

## Type Conversion in datatypes

```
var int a = 3
floatValue:= float64(a)
```

T(v) where the v is converted to the type T -> example var a int = 1, b=float64(a)

Type inference i.e automatically detect the type of the variable based on the shortHand variable declaration

a:= "this is a string" -> using type inference to detect the type of the variable 

## Data types

```
package main 
import (
"fmt"
""
)
// simiar to this multiple import even variable can be factored into blocks

var (
thisIsFalse bool = false
str string= "Hello Vinay"
)
```

## Introduction

NeoVim Shortcuts
```
:tabnext to move within the tabs
:tabnew filename.go creates a new go file
To compile the program withing the nvim :!go run % - this is the command
```

Exported Names - the exported name starts with a capital letter
func is the keyword to define a new function which can have a zero or more arguments

instead of func add(x int , y int ) int -> use this func add (x,y int ) int 

Multiple Return in function definition func addSome(x,y int) (string,int) {}

Short Variable Initialization ":="


