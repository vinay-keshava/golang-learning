package main

import "fmt"

func main() {

	// simple map of (int,int)->key,value
	var m map[int]int
	m = make(map[int]int) // a map has to be created before assiging values
	m[0] = 1
	fmt.Println(m[0])

	//map of string keys and int values
	m1 := make(map[string]int)
	m1["vinay"] = 1
	fmt.Println(m1["vinay"]) // here string is the key and integer is the value

	// map of String to struct
	type Subjects struct {
		subname string
		subcode int
	}
	m2 := make(map[string]Subjects)
	m2["8thSem"] = Subjects{"ComputerOrganization", 1871}
	fmt.Println(m2["8thSem"])
	fmt.Println(m2)

	//map of string to struct not using shorthand declaration
	var m3 = map[string]Subjects{
		"7thSem": Subjects{
			"Analog&DigitalElectronics", 1873,
		},
		"6thSem": Subjects{
			"IOT", 1874,
		},
	}
	fmt.Println(m3["6thSem"])

	// map of string to struct using normal declaration not shorthand without using the struct name while assigning keys and values without using make
	var m4 = map[string]Subjects{
		"5thSem": {"BigData", 144},
	}

	fmt.Println(m4["5thSem"])
}
