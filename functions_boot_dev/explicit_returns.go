package main

import "fmt"

func function2() (x, y int) {
	return 5, 6
}

// even though the function has named return values an explicit return is specified and x and y is not returned
func main() {
	fmt.Println("vim-go")
}
