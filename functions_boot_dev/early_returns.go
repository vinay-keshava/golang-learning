package main

import (
	"errors"
	"fmt"
)

func divide(dividend, divisor int) (float64, error) {
	if divisor == 0 {
		return 0.0, errors.New("Cannot divide by zero")

		// go supports early returns :wq
	}
	return float64(dividend) / float64(divisor), nil
}
func main() {
	fmt.Println("vim-go")

	fmt.Println(divide(10, 0))
	fmt.Println(divide(10, 20))
	fmt.Println(divide(30, 5))
}
