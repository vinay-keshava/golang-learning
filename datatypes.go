package main

import "fmt"

func main() {
	fmt.Println("vim-go")
	var someRandom bool = false
	fmt.Println(someRandom)

	shortHandBoolType := false
	fmt.Println(shortHandBoolType)

	// string examples
	var strString string = "fjdk"
	fmt.Println(strString)

	str2 := "Hello my name is vinay"
	fmt.Println(str2)

	//float values
	float := 45.334
	fmt.Println(float)

	var anotherFloat float64 = 90.290329
	fmt.Println(anotherFloat)

}
