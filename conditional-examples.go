package main

import (
	"fmt"
	"math"
	"runtime"
)

func main() {
	fmt.Println("vim-go")

	if 10 > 0 {
		fmt.Println("10 is greater than 0 ")
	}

	//if within a short statement
	if v := math.Sqrt(10); v > 0 {
		fmt.Println("If within a statement")
	}
	fmt.Println(runtime.GOARCH)
	fmt.Println(runtime.GOOS)
}
