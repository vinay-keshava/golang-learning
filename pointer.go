package main

import "fmt"

func main() {
	fmt.Println("Hello Pointers")
	i := 200
	fmt.Println(i)
	p := &i
	fmt.Println(&p)
	fmt.Println(*p, "Value of the pointer")
}
