package main

import "fmt"

type Sample struct {
	marks1   int
	sub_name string
}
type College struct {
	marks1       int
	sub1         string
	student_name string
	usn          int
}

func main() {
	fmt.Println("vim-go")
	fmt.Println(Sample{312, "Mathematics"})

	s := Sample{12, "Computer Science"}
	fmt.Println(s.sub_name)

	s.sub_name = "Design and  Analysis of Algorithms"
	fmt.Println(s.sub_name)

	// Structs with pointers
	s_pointer := &s
	s_pointer.sub_name = "Computer Organization"
	fmt.Println(s.sub_name)
	// here when an pointer is assessed to struct we can change the struct values using the pointers instead of directly using
	//struct variable this is example for struct pointer relation

	//Struct literals - >here we can specify the value for each struct values
	mvit := College{student_name: "vinay"}
	fmt.Println(mvit)
	mvit.usn = 041
	fmt.Println("After changing the struct literal")
	mvit.sub1 = "Data Science"
	fmt.Println(mvit)
}
