package main

import "fmt"

type Shape struct {
	X, Y float64
}

func callByvalue(s Shape) float64 {
	return s.X*s.X + s.Y*s.Y
}

func callByRef(s *Shape) float64 {
	return s.X*s.X + s.Y*s.Y
}
func main() {

	shape := Shape{20, 40}
	fmt.Println(callByRef(&shape))
	fmt.Println(callByvalue(shape))
}

//here call By value and reference is only for the function parameters
