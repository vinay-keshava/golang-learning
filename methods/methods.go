package main

import (
	"fmt"
	"math"
)

type Rectangle struct {
	x, y float64
}
type Circle struct {
	radius float64
}

// func (receiver type) methodName(parameters) return_type{}
/*
	here the receiver type is of two types
	1. Value Receiver -> similar to call by value
	2. Pointer Receiver -> similar to call by reference
*/
func (r Rectangle) Area() float64 {
	return r.x * r.y
}

//Value Receiver Method
/*
value receiver method receive a copy of the receiver but do not modify the original values
here the radius value is changed but the actual value remains same
*/
func (c Circle) Area_and_Perimeter() (float64, float64) {
	c.radius += 10.00
	area := math.Pi * math.Pow(c.radius, 2)
	perimeter := math.Pi * 2 * c.radius
	return area, perimeter
}

//Pointer Receiver Method
/*
In pointer receiver method the pointer of the value receiver is copied changing the values also modifies the original values
similar to call by reference in other languages
*/
func (c *Circle) Area_and_Perimeter_Pointer_Based() (float64, float64) {
	c.radius += 10.00
	area := math.Pi * math.Pow(c.radius, 2)
	perimeter := math.Pi * 2 * c.radius
	return area, perimeter
}

// creating an alias for builtin dataypes using type
type MyInt int

func (i MyInt) randomFunction() int {
	return 1
}
func main() {
	//methods in golang can be of two different types since golang doesn't have classes functions can be declared as type
	first_rectangle := Rectangle{10.00, 20.00}
	fmt.Println(first_rectangle.Area())

	radius := 14.00
	first_circle := Circle{radius}
	fmt.Println(first_circle.Area_and_Perimeter())

	fmt.Println("Before calling pointer receiver based method value of radius", first_circle.radius)
	fmt.Println(first_circle.Area_and_Perimeter_Pointer_Based())
	fmt.Println("After calling pointer receiver based method value of radius is:", first_circle.radius)
}
