package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("vim-go")

	//similar to if within short statement there can be switch with short statement too
	switch i := runtime.GOARCH; i {
	case "amd64":
		fmt.Println("This is amd64")
	default:
		fmt.Println("Some default statement")
	}
}
