package main

import "fmt"

func main() {

	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}

	//skipping the init and post in for loop of go
	j := 10
	for j > 0 {
		fmt.Println(j)
		j -= 1
	}

	// for is go's while loop
	for j == 0 {
		fmt.Println("This is for's while loop in golang no different than for ")
	}
}
