package main

import "fmt"

func sample() {
	fmt.Println("Inside the function sample")
}

func addSomeShit(first_number int, second_num int) int {
	return first_number + second_num
}

//other function definitions - >
func addSomeMoreShit(first, second int) int {
	return first + second
}

//multiple return statements
func addSomeMoreReturnMoreShit(first int, second string) (string, int) {
	return second, first
}

func withNamedReturnValues(sum int) (x, y, z int) {
	x = sum + 1
	y = sum + 2
	z = sum + 3
	return
}
func main() {
	fmt.Println("vim-go")
	sample()
	fmt.Println(addSomeShit(20, 20))
	fmt.Println(addSomeMoreShit(20, 30))
	fmt.Println(addSomeMoreReturnMoreShit(10, "Vinay"))
	fmt.Println(withNamedReturnValues(30))
}
