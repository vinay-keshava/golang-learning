package main

import "fmt"

// this is a variadic function where any number of trailing arguments can be called fmt.Println is a common example of variadic function
func sum(nums ...int) {

	total := 0
	for _, v := range nums {
		total += v
	}
	fmt.Println(total)
}
func main() {

	sum(1, 3, 4, 5)
	sum(12, 3)
	fmt.Println("vim-go")

}
